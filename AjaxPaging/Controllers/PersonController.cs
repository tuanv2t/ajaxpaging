﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AjaxPaging.Helpers;
using AjaxPaging.Models;
namespace AjaxPaging.Controllers
{
    public class PersonController : Controller
    {
        public const int PageSize = 5;
        private List<Person> data = new List<Person>();
        public PersonController()
        {
            data.Add(new Person { PersonId = 1, Forename = "A", Surname = "B" });
            data.Add(new Person { PersonId = 2, Forename = "A1", Surname = "B1" });
            data.Add(new Person { PersonId = 3, Forename = "A2", Surname = "B2" });
            data.Add(new Person { PersonId = 4, Forename = "C", Surname = "C" });
            data.Add(new Person { PersonId = 5, Forename = "C1", Surname = "C1" });
            data.Add(new Person { PersonId = 6, Forename = "C2", Surname = "C2" });
            data.Add(new Person { PersonId = 7, Forename = "C2", Surname = "C2" });
            data.Add(new Person { PersonId = 8, Forename = "D", Surname = "E" });
            data.Add(new Person { PersonId = 9, Forename = "D1", Surname = "E1" });
            data.Add(new Person { PersonId = 10, Forename = "D2", Surname = "E2" });
            data.Add(new Person { PersonId = 11, Forename = "F1", Surname = "G1" });
            data.Add(new Person { PersonId = 12, Forename = "F2", Surname = "G2" });
            data.Add(new Person { PersonId = 13, Forename = "H1", Surname = "I1" });
            data.Add(new Person { PersonId = 14, Forename = "H2", Surname = "I2" });
            data.Add(new Person { PersonId = 15, Forename = "J1", Surname = "K1" });
            data.Add(new Person { PersonId = 16, Forename = "L1", Surname = "M1" });
        }
        [HttpPost]
        public ActionResult PersonListPaged(PagedRequest request)
        {
            if (request.Page == null)
            {
                request.Page = 1;
            }
            var people = new PagedData<Person>();
            people.Request = request;
            people.Request.PageSize = PageSize;
            var query = data.AsQueryable();//Use EntityFramework 
            people.TotalEntries = query.Count();

            if (!string.IsNullOrWhiteSpace(request.KeySearch))
            {
                var keySearch =request.KeySearch.Trim();
                query = query.Where(x => x.Forename.Contains(keySearch)||x.Surname.Contains(keySearch));//demo how to search 
            }
            //people.Data = query.OrderBy(p => p.Surname).Skip(PageSize * (request.Page.Value - 1)).Take(PageSize).ToList();
            //Try to sort
            if (request.SortRequest == null)
            {
                request.SortRequest = new List<SortItem>();
            }
            if (request.SortRequest.Count == 0)
            {
                //Default sort 
                request.SortRequest.Add(new SortItem { SortData = "personid", SortDir = SortItem.SortAsc });
            }
           
            //Sort single column first 
            if (request.SortRequest[0].SortData == "personid")
            {
                if(request.SortRequest[0].SortDir== SortItem.SortAsc)
                {
                    query = query.OrderBy(p => p.PersonId);
                }
                else
                {
                    query = query.OrderByDescending(p => p.PersonId);
                }
                   
            }
            if (request.SortRequest[0].SortData == "forename")
            {
                if (request.SortRequest[0].SortDir == SortItem.SortAsc)
                {
                    query = query.OrderBy(p => p.Forename);
                }
                else
                {
                    query = query.OrderByDescending(p => p.Forename);
                }

            }
            if (request.SortRequest[0].SortData == "surname")
            {
                if (request.SortRequest[0].SortDir == SortItem.SortAsc)
                {
                    query = query.OrderBy(p => p.Surname);
                }
                else
                {
                    query = query.OrderByDescending(p => p.Surname);
                }

            }
            if (request.SortRequest[0].SortData == "location")
            {
                if (request.SortRequest[0].SortDir == SortItem.SortAsc)
                {
                    query = query.OrderBy(p => p.Country);
                }
                else
                {
                    query = query.OrderByDescending(p => p.Country);
                }

            }
            if (request.SortRequest.Count == 1)
            {
                if (string.IsNullOrEmpty(request.SortRequest[0].SortData))
                {
                    request.SortRequest[0].SortData = "personid";
                }
                if (string.IsNullOrEmpty(request.SortRequest[0].SortDir))
                {
                    request.SortRequest[0].SortDir = SortItem.SortAsc;
                }
            }
            people.Data = query.Skip(PageSize * (request.Page.Value - 1)).Take(PageSize).ToList();
            people.FilterdEntries = query.Count();
            people.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)people.FilterdEntries / PageSize));
            people.CurrentPage = request.Page.Value;
           

            return PartialView("_PersonListPaged", people);
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PersonList()
        {
            return PartialView();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}