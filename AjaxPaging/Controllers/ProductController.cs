﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AjaxPaging.Helpers;
using AjaxPaging.Models;
namespace AjaxPaging.Controllers
{
    public class ProductController : Controller
    {
        public const int PageSize = 5;
        private List<Product> data = new List<Product>();
        public ProductController()
        {
            data.Add(new Product { ProductId = 1, ProductName = "ProductA", ProductCategory = "CatB" });
            data.Add(new Product { ProductId = 2, ProductName = "ProductA1", ProductCategory = "CatB1" });
            data.Add(new Product { ProductId = 3, ProductName = "ProductA2", ProductCategory = "CatB2" });
            data.Add(new Product { ProductId = 4, ProductName = "ProductC", ProductCategory = "CatC" });
            data.Add(new Product { ProductId = 5, ProductName = "ProductC1", ProductCategory = "CatC1" });
            data.Add(new Product { ProductId = 6, ProductName = "ProductC2", ProductCategory = "CatC2" });
            data.Add(new Product { ProductId = 7, ProductName = "ProductC2", ProductCategory = "CatC2" });
            data.Add(new Product { ProductId = 8, ProductName = "ProductD", ProductCategory = "CatE" });
            data.Add(new Product { ProductId = 9, ProductName = "ProductD1", ProductCategory = "CatE1" });
            data.Add(new Product { ProductId = 10, ProductName = "ProductD2", ProductCategory = "CatE2" });
            data.Add(new Product { ProductId = 11, ProductName = "ProductF1", ProductCategory = "CatG1" });
            data.Add(new Product { ProductId = 12, ProductName = "ProductF2", ProductCategory = "CatG2" });
            data.Add(new Product { ProductId = 13, ProductName = "ProductH1", ProductCategory = "CatI1" });
            data.Add(new Product { ProductId = 14, ProductName = "ProductH2", ProductCategory = "CatI2" });
            data.Add(new Product { ProductId = 15, ProductName = "ProductJ1", ProductCategory = "CatK1" });
            data.Add(new Product { ProductId = 16, ProductName = "ProductL1", ProductCategory = "CatM1" });
        }
        [HttpPost]
        public ActionResult ProductListPaged(PagedRequest request)
        {
            if (request.Page == null)
            {
                request.Page = 1;
            }
            var people = new PagedData<Product>();
            people.Request = request;
            people.Request.PageSize = PageSize;
            var query = data.AsQueryable();//Use EntityFramework 
            people.TotalEntries = query.Count();

            if (!string.IsNullOrWhiteSpace(request.KeySearch))
            {
                var keySearch =request.KeySearch.Trim();
                query = query.Where(x => x.ProductName.Contains(keySearch)||x.ProductCategory.Contains(keySearch));//demo how to search 
            }
            people.Data = query.OrderBy(p => p.ProductCategory).Skip(PageSize * (request.Page.Value - 1)).Take(PageSize).ToList();
            people.FilterdEntries = query.Count();
            people.NumberOfPages = Convert.ToInt32(Math.Ceiling((double)people.FilterdEntries / PageSize));
            people.CurrentPage = request.Page.Value;
           

            return PartialView("_ProductListPaged", people);
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ProductList()
        {
            return PartialView();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}