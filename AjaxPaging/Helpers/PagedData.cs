﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AjaxPaging.Helpers
{
    public class PagedData<T> where T : class
    {
       public PagedRequest Request { get; set; }
        public IEnumerable<T> Data { get; set; }
        public int NumberOfPages { get; set; }
        public int CurrentPage { get; set; }
        public int TotalEntries { get; set; }
        public int FilterdEntries { get; set; }
        public int From { get; set; }
        public int To { get; set; }
    }
}