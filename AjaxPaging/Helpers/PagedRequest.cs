﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AjaxPaging.Helpers
{
    public class PagedRequest
    {
        public PagedRequest()
        {
            SortRequest = new List<SortItem>();
        }
        public int? Page { get; set; }
        public string Guid { get; set; }
        public string KeySearch { get; set; }
        public int PageSize { get; set; }
        public string AjaxToGetTableBody { get; set; }
        public List<SortItem> SortRequest { get; set; }
    }
}