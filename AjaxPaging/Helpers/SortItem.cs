﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AjaxPaging.Helpers
{
    public class SortItem
    {
        public const string SortAsc = "asc";
        public const string SortDesc = "desc";
        public string SortData { get; set; }
        public string SortDir { get; set; }
    }
}