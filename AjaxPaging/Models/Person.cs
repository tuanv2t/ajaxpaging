﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AjaxPaging.Models
{
    public class Person
    {
        public int PersonId { get; set; }

        public string Forename { get; set; }

        public string Surname { get; set; }

        public string Country { get; set; }
    }
}